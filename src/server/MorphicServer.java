package server;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import robot.SmartRobot;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.ServerRunner;

public class MorphicServer extends NanoHTTPD {
	SmartRobot robot = null;
	// for Chrome's built-in offsets
	int xOffset = 7;
	int yOffset = 69;
	int xOffsetMeta = 0;
	int yOffsetMeta = 62;
	// synchronizing from client
	int xOffsetStart = 500;
	int yOffsetStart = 200;
	NanoHTTPD.Response response;

	public MorphicServer() {
		super(8080);
	}

	public static void main(String[] args) {
		try {
			Desktop.getDesktop().browse(
					new URL("http://localhost:8080").toURI());
		} catch (Exception e) {
			e.printStackTrace();
		}
		ServerRunner.run(MorphicServer.class);
	}

	@Override
	public Response serve(IHTTPSession session) {
		Method method = session.getMethod();
		String uri = session.getUri();
		String msg = "";
		Map<String, String> parms = session.getParms();

		if (uri.startsWith("/robot")) {
			try {
				getRobot();
			} catch (AWTException e1) {
				e1.printStackTrace();
			}

			System.out.println(method + " '" + uri + "' ");
			if (parms.get("click") != null) {
				if (parms.get("click").equals("true")) {
					robot.mousePress(InputEvent.BUTTON1_MASK);
					robot.mouseRelease(InputEvent.BUTTON1_MASK);
					msg += "click.OK";
				} else {
					msg += "click.ERROR";
					System.out.println(msg);
				}
			} else if (parms.get("mouseMoveSimple") != null) {
				/*
				 * JavaScript(jQuery): $.post(
				 * '/robot?mouseMoveSimple={"x":1000,"y":200}', function( data )
				 * { console.log(data); });
				 */
				JSONObject mouseMoveSimple = new JSONObject(
						parms.get("mouseMoveSimple"));
				if (mouseMoveSimple.getInt("x") >= 0
						&& mouseMoveSimple.getInt("y") >= 0) {
					int x = mouseMoveSimple.getInt("x") + xOffset;
					int y = mouseMoveSimple.getInt("y") + yOffset;
					robot.mouseMove(x, y);
					System.out.println("Mouse moved simply to x:" + x + "; y:"
							+ y);
					msg += "mouseMoveSimple.OK";
				} else {
					msg += "mouseMoveSimple.ERROR";
					System.out.println(msg);
				}
			} else if (parms.get("mouseMove") != null) {
				JSONObject mouseMove = new JSONObject(parms.get("mouseMove"));
				if (mouseMove.getInt("x") >= 0 && mouseMove.getInt("y") >= 0) {
					int x = mouseMove.getInt("x") + xOffset;
					int y = mouseMove.getInt("y") + yOffset;
					Point mousePosition = MouseInfo.getPointerInfo()
							.getLocation();
					robot.mouseGlide(mousePosition.x, mousePosition.y, x, y,
							500, 100);
					System.out.println("Mouse moved to x:" + x + "; y:" + y);
					msg += "mouseMove.OK";
				} else {
					msg += "mouseMove.ERROR";
					System.out.println(msg);
				}
			} else if (parms.get("pressAt") != null) {
				JSONObject pressAt = new JSONObject(parms.get("pressAt"));
				if (pressAt.getInt("x") >= 0 && pressAt.getInt("y") >= 0) {
					int x = pressAt.getInt("x") + xOffset;
					int y = pressAt.getInt("y") + yOffset;
					Point mousePosition = MouseInfo.getPointerInfo()
							.getLocation();
					robot.mouseGlide(mousePosition.x, mousePosition.y, x, y,
							500, 100);
					robot.mousePress(InputEvent.BUTTON1_MASK);
					robot.mouseRelease(InputEvent.BUTTON1_MASK);
					System.out.println("Mouse moved and clicked at x:" + x
							+ "; y:" + y);
					msg += "pressAt.OK";
				} else {
					msg += "pressAt.ERROR";
					System.out.println(msg);
				}
			} else if (parms.get("type") != null) {
				String text = parms.get("type");
				robot.type(text, robot);
				System.out.println("Typed: " + text);
				msg += "type.OK";
			} else if (parms.get("dragDrop") != null) {
				JSONObject dragDrop = new JSONObject(parms.get("dragDrop"));
				if (dragDrop.getInt("x1") >= 0 && dragDrop.getInt("y1") >= 0
						&& dragDrop.getInt("x2") >= 0
						&& dragDrop.getInt("y2") >= 0) {
					int x1 = dragDrop.getInt("x1") + xOffset;
					int y1 = dragDrop.getInt("y1") + yOffset;
					int x2 = dragDrop.getInt("x2") + xOffset;
					int y2 = dragDrop.getInt("y2") + yOffset;
					Point mousePosition = MouseInfo.getPointerInfo()
							.getLocation();
					robot.mouseGlide(mousePosition.x, mousePosition.y, x1, y1,
							500, 100);
					robot.mousePress(InputEvent.BUTTON1_MASK);
					robot.mouseGlide(x1, y1, x2, y2, 500, 100);
					robot.mouseRelease(InputEvent.BUTTON1_MASK);
					System.out.println("Dragged and dropped from x1:" + x1
							+ "; y1:" + y1 + " to x2: " + x2 + "; y2:" + y2);
					msg += "dragDrop.OK";
				} else {
					msg += "dragDrop.ERROR";
					System.out.println(msg);
				}
			} else if (parms.get("mouseMoveSimpleMeta") != null) {
				JSONObject mouseMoveSimple = new JSONObject(
						parms.get("mouseMoveSimpleMeta"));
				JSONArray moveArray = mouseMoveSimple.getJSONArray("moveArray");
				for (int i = 0; i < moveArray.length(); i++) {
					JSONObject obj = (JSONObject) moveArray.get(i);
					int x = obj.getInt("x") + xOffsetMeta;
					int y = obj.getInt("y") + yOffsetMeta;
					robot.mouseMove(x, y);
					System.out.println("Mouse moved simply to x:" + x + "; y:"
							+ y);
					msg += "mouseMovemouseMoveSimpleMetaSimple.OK ";
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else if (parms.get("synchronizeOffsets") != null) {
				Point mousePosition = MouseInfo.getPointerInfo().getLocation();
				JSONObject synchronizeOffsets = new JSONObject(
						parms.get("synchronizeOffsets"));
				if (synchronizeOffsets.getInt("x") == 0
						&& synchronizeOffsets.getInt("y") == 0) {
					robot.mouseGlide(mousePosition.x, mousePosition.y,
							xOffsetStart, yOffsetStart, 500, 100);
					// msg +=
					// "{\"xOffsetStart\":"+xOffsetStart+", \"yOffsetStart\":" +
					// yOffsetStart + "}";
					msg += "synch.step1.OK";
				} else if (synchronizeOffsets.getInt("x") >= 0
						&& synchronizeOffsets.getInt("y") >= 0) {
					int x = synchronizeOffsets.getInt("x");
					int y = synchronizeOffsets.getInt("y");

					xOffset = Math.abs(x - xOffsetStart);
					yOffset = Math.abs(y - yOffsetStart);
					/*
					 * robot.mouseGlide(mousePosition.x, mousePosition.y, 300,
					 * 250, 500, 100);
					 * robot.mousePress(InputEvent.BUTTON1_MASK);
					 * robot.mouseRelease(InputEvent.BUTTON1_MASK);
					 */
					System.out.println("Synchronization finished, new xOffset: "
							+ xOffset + "; yOffset: " + yOffset);
					//msg += "synch.step2.OK";
					msg += "{\"xOffset\":" + xOffset + ",\"yOffset\":" + yOffset + ",\"message\":\"synch.step2.OK\"}";
				} else {
					msg += "synch.ERROR";
					System.out.println(msg);
				}
			}
		} else if (uri.startsWith("/test")) {
			Map<String, String> MIME_TYPES = new HashMap<String, String>() {
				{
					put("css", "text/css");
					put("htm", "text/html");
					put("html", "text/html");
					put("xml", "text/xml");
					put("java", "text/x-java-source, text/java");
					put("md", "text/plain");
					put("txt", "text/plain");
					put("asc", "text/plain");
					put("gif", "image/gif");
					put("jpg", "image/jpeg");
					put("jpeg", "image/jpeg");
					put("png", "image/png");
					put("mp3", "audio/mpeg");
					put("m3u", "audio/mpeg-url");
					put("mp4", "video/mp4");
					put("ogv", "video/ogg");
					put("flv", "video/x-flv");
					put("mov", "video/quicktime");
					put("swf", "application/x-shockwave-flash");
					put("js", "application/javascript");
					put("pdf", "application/pdf");
					put("doc", "application/msword");
					put("ogg", "application/x-ogg");
					put("zip", "application/octet-stream");
					put("exe", "application/octet-stream");
					put("class", "application/octet-stream");
				}
			};
			return new NanoHTTPD.Response(Response.Status.OK, MIME_HTML,
					"html/robot.html");
		} else {
			msg += "<html><header><script src='//code.jquery.com/jquery-1.11.0.min.js'></script></header><body><h1>";
			msg += "Morphic local robot server started!";
			msg += "</h1><p>You can make AJAX calls to the server similar like this:</p>";
			msg += "<p> $.post('http://localhost:8080/robot?mouseMove={\"x\":50,\"y\":60}',function(response){console.log(response);});</p>";
			msg += "</body></html>";
		}
		response = new NanoHTTPD.Response(msg);
		// XSS
		response.addHeader("Access-Control-Allow-Origin", "*");
		return response;
	}

	SmartRobot getRobot() throws AWTException {
		if (robot == null) {
			robot = new SmartRobot();
		}
		return robot;
	}

	public static void openWebpage(URI uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop()
				: null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void openWebpage(URL url) {
		try {
			openWebpage(url.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
}
