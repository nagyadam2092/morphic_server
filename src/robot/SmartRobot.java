package robot;

import static java.awt.event.KeyEvent.VK_0;
import static java.awt.event.KeyEvent.VK_1;
import static java.awt.event.KeyEvent.VK_2;
import static java.awt.event.KeyEvent.VK_3;
import static java.awt.event.KeyEvent.VK_4;
import static java.awt.event.KeyEvent.VK_5;
import static java.awt.event.KeyEvent.VK_6;
import static java.awt.event.KeyEvent.VK_7;
import static java.awt.event.KeyEvent.VK_8;
import static java.awt.event.KeyEvent.VK_9;
import static java.awt.event.KeyEvent.VK_A;
import static java.awt.event.KeyEvent.VK_AMPERSAND;
import static java.awt.event.KeyEvent.VK_ASTERISK;
import static java.awt.event.KeyEvent.VK_AT;
import static java.awt.event.KeyEvent.VK_B;
import static java.awt.event.KeyEvent.VK_BACK_QUOTE;
import static java.awt.event.KeyEvent.VK_BACK_SLASH;
import static java.awt.event.KeyEvent.VK_C;
import static java.awt.event.KeyEvent.VK_CIRCUMFLEX;
import static java.awt.event.KeyEvent.VK_CLOSE_BRACKET;
import static java.awt.event.KeyEvent.VK_COLON;
import static java.awt.event.KeyEvent.VK_COMMA;
import static java.awt.event.KeyEvent.VK_D;
import static java.awt.event.KeyEvent.VK_DOLLAR;
import static java.awt.event.KeyEvent.VK_E;
import static java.awt.event.KeyEvent.VK_ENTER;
import static java.awt.event.KeyEvent.VK_EQUALS;
import static java.awt.event.KeyEvent.VK_EXCLAMATION_MARK;
import static java.awt.event.KeyEvent.VK_F;
import static java.awt.event.KeyEvent.VK_G;
import static java.awt.event.KeyEvent.VK_GREATER;
import static java.awt.event.KeyEvent.VK_H;
import static java.awt.event.KeyEvent.VK_I;
import static java.awt.event.KeyEvent.VK_J;
import static java.awt.event.KeyEvent.VK_K;
import static java.awt.event.KeyEvent.VK_L;
import static java.awt.event.KeyEvent.VK_LEFT_PARENTHESIS;
import static java.awt.event.KeyEvent.VK_LESS;
import static java.awt.event.KeyEvent.VK_M;
import static java.awt.event.KeyEvent.VK_MINUS;
import static java.awt.event.KeyEvent.VK_N;
import static java.awt.event.KeyEvent.VK_NUMBER_SIGN;
import static java.awt.event.KeyEvent.VK_O;
import static java.awt.event.KeyEvent.VK_OPEN_BRACKET;
import static java.awt.event.KeyEvent.VK_P;
import static java.awt.event.KeyEvent.VK_PERIOD;
import static java.awt.event.KeyEvent.VK_PLUS;
import static java.awt.event.KeyEvent.VK_Q;
import static java.awt.event.KeyEvent.VK_QUOTE;
import static java.awt.event.KeyEvent.VK_QUOTEDBL;
import static java.awt.event.KeyEvent.VK_R;
import static java.awt.event.KeyEvent.VK_RIGHT_PARENTHESIS;
import static java.awt.event.KeyEvent.VK_S;
import static java.awt.event.KeyEvent.VK_SEMICOLON;
import static java.awt.event.KeyEvent.VK_SHIFT;
import static java.awt.event.KeyEvent.VK_SLASH;
import static java.awt.event.KeyEvent.VK_SPACE;
import static java.awt.event.KeyEvent.VK_T;
import static java.awt.event.KeyEvent.VK_TAB;
import static java.awt.event.KeyEvent.VK_U;
import static java.awt.event.KeyEvent.VK_UNDERSCORE;
import static java.awt.event.KeyEvent.VK_V;
import static java.awt.event.KeyEvent.VK_W;
import static java.awt.event.KeyEvent.VK_X;
import static java.awt.event.KeyEvent.VK_Y;
import static java.awt.event.KeyEvent.VK_Z;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class SmartRobot extends Robot {
	int typeDelay = 100;

	public SmartRobot() throws AWTException {
		super();
	}

	// http://stackoverflow.com/questions/9387483/how-to-move-a-mouse-smoothly-throughout
	public void mouseGlide(int x1, int y1, int x2, int y2, int t, int n) {
		try {

			double dx = (x2 - x1) / ((double) n);
			double dy = (y2 - y1) / ((double) n);
			double dt = t / ((double) n);
			for (int step = 1; step <= n; step++) {
				Thread.sleep((int) dt);
				mouseMove((int) (x1 + dx * step), (int) (y1 + dy * step));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// http://stackoverflow.com/questions/1248510/convert-string-to-keyevents
	public void type(CharSequence characters, Robot robot) {
		int length = characters.length();
		for (int i = 0; i < length; i++) {
			char character = characters.charAt(i);
			type(character, robot);
			delay(typeDelay);
		}
	}

	public void type(char character, Robot robot) {
		switch (character) {
		case 'a':
			doType(robot, VK_A);
			break;
		case 'b':
			doType(robot, VK_B);
			break;
		case 'c':
			doType(robot, VK_C);
			break;
		case 'd':
			doType(robot, VK_D);
			break;
		case 'e':
			doType(robot, VK_E);
			break;
		case 'f':
			doType(robot, VK_F);
			break;
		case 'g':
			doType(robot, VK_G);
			break;
		case 'h':
			doType(robot, VK_H);
			break;
		case 'i':
			doType(robot, VK_I);
			break;
		case 'j':
			doType(robot, VK_J);
			break;
		case 'k':
			doType(robot, VK_K);
			break;
		case 'l':
			doType(robot, VK_L);
			break;
		case 'm':
			doType(robot, VK_M);
			break;
		case 'n':
			doType(robot, VK_N);
			break;
		case 'o':
			doType(robot, VK_O);
			break;
		case 'p':
			doType(robot, VK_P);
			break;
		case 'q':
			doType(robot, VK_Q);
			break;
		case 'r':
			doType(robot, VK_R);
			break;
		case 's':
			doType(robot, VK_S);
			break;
		case 't':
			doType(robot, VK_T);
			break;
		case 'u':
			doType(robot, VK_U);
			break;
		case 'v':
			doType(robot, VK_V);
			break;
		case 'w':
			doType(robot, VK_W);
			break;
		case 'x':
			doType(robot, VK_X);
			break;
		case 'y':
			doType(robot, VK_Y);
			break;
		case 'z':
			doType(robot, VK_Z);
			break;
		case 'A':
			doType(robot, VK_SHIFT, VK_A);
			break;
		case 'B':
			doType(robot, VK_SHIFT, VK_B);
			break;
		case 'C':
			doType(robot, VK_SHIFT, VK_C);
			break;
		case 'D':
			doType(robot, VK_SHIFT, VK_D);
			break;
		case 'E':
			doType(robot, VK_SHIFT, VK_E);
			break;
		case 'F':
			doType(robot, VK_SHIFT, VK_F);
			break;
		case 'G':
			doType(robot, VK_SHIFT, VK_G);
			break;
		case 'H':
			doType(robot, VK_SHIFT, VK_H);
			break;
		case 'I':
			doType(robot, VK_SHIFT, VK_I);
			break;
		case 'J':
			doType(robot, VK_SHIFT, VK_J);
			break;
		case 'K':
			doType(robot, VK_SHIFT, VK_K);
			break;
		case 'L':
			doType(robot, VK_SHIFT, VK_L);
			break;
		case 'M':
			doType(robot, VK_SHIFT, VK_M);
			break;
		case 'N':
			doType(robot, VK_SHIFT, VK_N);
			break;
		case 'O':
			doType(robot, VK_SHIFT, VK_O);
			break;
		case 'P':
			doType(robot, VK_SHIFT, VK_P);
			break;
		case 'Q':
			doType(robot, VK_SHIFT, VK_Q);
			break;
		case 'R':
			doType(robot, VK_SHIFT, VK_R);
			break;
		case 'S':
			doType(robot, VK_SHIFT, VK_S);
			break;
		case 'T':
			doType(robot, VK_SHIFT, VK_T);
			break;
		case 'U':
			doType(robot, VK_SHIFT, VK_U);
			break;
		case 'V':
			doType(robot, VK_SHIFT, VK_V);
			break;
		case 'W':
			doType(robot, VK_SHIFT, VK_W);
			break;
		case 'X':
			doType(robot, VK_SHIFT, VK_X);
			break;
		case 'Y':
			doType(robot, VK_SHIFT, VK_Y);
			break;
		case 'Z':
			doType(robot, VK_SHIFT, VK_Z);
			break;
		case '`':
			doType(robot, VK_BACK_QUOTE);
			break;
		case '0':
			doType(robot, VK_0);
			break;
		case '1':
			doType(robot, VK_1);
			break;
		case '2':
			doType(robot, VK_2);
			break;
		case '3':
			doType(robot, VK_3);
			break;
		case '4':
			doType(robot, VK_4);
			break;
		case '5':
			doType(robot, VK_5);
			break;
		case '6':
			doType(robot, VK_6);
			break;
		case '7':
			doType(robot, VK_7);
			break;
		case '8':
			doType(robot, VK_8);
			break;
		case '9':
			doType(robot, VK_9);
			break;
		case '-':
			doType(robot, VK_MINUS);
			break;
		case '=':
			doType(VK_SHIFT, VK_7, robot);
			break;
		case '~':
			doType(robot, VK_SHIFT, VK_BACK_QUOTE);
			break;
		case '!':
			doType(robot, VK_EXCLAMATION_MARK);
			break;
		case '@':
			doType(robot, VK_AT);
			break;
		case '#':
			doType(KeyEvent.VK_CONTROL, KeyEvent.VK_ALT, VK_X, robot);
			break;
		case '$':
			robot.keyPress(VK_DOLLAR);
			robot.keyRelease(VK_DOLLAR);
			break;
		case '%':
			doType(robot, VK_SHIFT, VK_5);
			break;
		case '^':
			doType(robot, VK_CIRCUMFLEX);
			break;
		case '&':
			doType(robot, VK_AMPERSAND);
			break;
		case '*':
			doType(robot, VK_ASTERISK);
			break;
		case '(':
			doType(VK_SHIFT, VK_8, robot);
			break;
		case ')':
			doType(VK_SHIFT, VK_9, robot);
			break;
		case '_':
			doType(robot, VK_UNDERSCORE);
			break;
		case '+':
			doType(robot, VK_PLUS);
			break;
		case '\t':
			doType(robot, VK_TAB);
			break;
		case '\n':
			doType(robot, VK_ENTER);
			break;
		case '[':
			doType(robot, VK_OPEN_BRACKET);
			break;
		case ']':
			doType(robot, VK_CLOSE_BRACKET);
			break;
		case '\\':
			doType(robot, VK_BACK_SLASH);
			break;
		case '{':
			doType(robot, VK_SHIFT, VK_OPEN_BRACKET);
			break;
		case '}':
			doType(robot, VK_SHIFT, VK_CLOSE_BRACKET);
			break;
		case '|':
			doType(robot, VK_SHIFT, VK_BACK_SLASH);
			break;
		case ';':
			doType(robot, VK_SEMICOLON);
			break;
		case ':':
			doType(robot, VK_COLON);
			break;
		case '\'':
			doType(robot, VK_QUOTE);
			break;
		case '"':
			doType(VK_SHIFT, VK_2, robot);
			break;
		case ',':
			doType(robot, VK_COMMA);
			break;
		case '<':
			doType(robot, VK_LESS);
			break;
		case '.':
			doType(robot, VK_PERIOD);
			break;
		case '>':
			doType(KeyEvent.VK_CONTROL, KeyEvent.VK_ALT, VK_Y, robot);
			break;
		case '/':
			doType(robot, VK_SLASH);
			break;
		case '?':
			doType(robot, VK_SHIFT, VK_SLASH);
			break;
		case ' ':
			doType(robot, VK_SPACE);
			break;
		default:
			throw new IllegalArgumentException("Cannot type character "
					+ character);
		}
	}

	private void doType(Robot robot, int... keyCodes) {
		doType(keyCodes, 0, keyCodes.length, robot);
	}

	private void doType(int[] keyCodes, int offset, int length, Robot robot) {
		if (length == 0) {
			return;
		}
		//System.out.println("keyCodes[offset]: " + keyCodes[offset]);
		robot.keyPress(keyCodes[offset]);
		doType(keyCodes, offset + 1, length - 1, robot);
		robot.keyRelease(keyCodes[offset]);
	}

	private void doType(int keyCode1, int keyCode2, int keyCode3, Robot robot) {
		try {
			robot.keyPress(keyCode1);
			robot.keyPress(keyCode2);
			robot.keyPress(keyCode3);
			robot.keyRelease(keyCode3);
			robot.keyRelease(keyCode2);
			robot.keyRelease(keyCode1);
		} catch (Exception e) {
			System.out.println("Invalid key code(s) for above character");
		}
	}
	
	private void doType(int keyCode1, int keyCode2, Robot robot) {
		try {
			robot.keyPress(keyCode1);
			robot.keyPress(keyCode2);
			robot.keyRelease(keyCode2);
			robot.keyRelease(keyCode1);
		} catch (Exception e) {
			System.out.println("Invalid key code(s) for above character");
		}
	}
	// END
	// http://stackoverflow.com/questions/1248510/convert-string-to-keyevents
}
